window.onload = (event) => {
    console.log({location: window.location});
    if (isInNM2()) {
        nm2GetSettings().then(async (data) => {
            // always eventually refresh the page so that on/off settings changes get picked up
            countdownToNewLocation(isInClanArea() ? 1 : data.stayLength, data.area);

            if (data.isOn) {
               await runBot(data);
            }
        });
    }
};

const runBot = async (data) => {
    if (isLoggedOut()) {
        await logIn();
    } else if (isInMyTeamMenu()) {
        await watchReplays();
    } else {
        await handleLoggedInScenario(data);
    }
};

const watchReplays = async (data) => {
    console.log("Watching replays!");
    const replays = document.getElementsByClassName('js-replay');
    const seenReplays = await nm2GetAllReplays();
    const seenReplayIds = seenReplays.map(replay => replay.replayid);

    for (const index in replays) {
        button = replays[index];
        
        try{
            const replayId = parseInt(button.getAttribute('data-replayid'));
            if (!seenReplayIds.includes(replayId)) {
                button.click();
                await watchReplay();
            }
        } catch (err) {
            // do nothing
        }
    };
};

const watchReplay = async () => {
    console.log("recording replay....");
    await skipBattle();
    await sleep(3000);
    const totalExp = getTotalExperience();
    const teamId = getEnemyReplayTeamId();
    const replayId = getReplayId();
    console.log({teamId, replayId, totalExp});
    return nm2PutReplay(teamId, replayId, totalExp);
};

const getTeamIdForSide = (isLeft) => {
    return parseInt(
        document.getElementsByClassName(isLeft ? '-side-left' : '-side-right')[2]
        .getElementsByClassName('pm-battle-side__name')[0]
        .getElementsByTagName('a')[0]
        .getAttribute('href')
        .split('/')
        .at(-1)
    );
};

const isLeftSideReplay = () => {
    return getTeamIdForSide(true) === 101;
};

const getTotalExperience = () => {
    const battleSide = document.getElementsByClassName('-battle-side-' + (isLeftSideReplay() ? 'left' : 'right'));
    const experianceContainers = battleSide[battleSide.length - 1].getElementsByClassName('c-card__exp-gain-nr');

    let totalExp = 0;
    for(const index in experianceContainers) {
        const expElement = experianceContainers[index];
        try {
            const exp = parseInt(expElement.getElementsByTagName('b')[0].innerText);
            totalExp += exp;
        } catch (err) {
            // do nothing
        }
    }
    return totalExp;
};

const getEnemyReplayTeamId = () => {
    return getTeamIdForSide(!isLeftSideReplay())
};

const getReplayId = () => {
    return parseInt(window.location.href.split('/').at(-1));
};

const skipBattle = async () => {
    const skipButton = getSkipButton();
    const rewatchButton = getRewatchButton();

    if (skipButton) {
        skipButton.click();
    }

    if (!rewatchButton) {
        await sleep(1000);
        await skipBattle();
    }
};

const getSkipButton = () => {
    return document.getElementsByClassName('js-battle-skip')[0];
};

const getRewatchButton = () => {
    return document.getElementsByClassName('js-battle-rewatch')[0];
};

const handleLoggedInScenario = async (data) => {
    if (isInArena()) {
        await beginArenaLoops(data.arenaEnergyCap);
    } else if (isInWorld()) {
        await beginWorldLoops(data.worldEnergyCap, data.mission);
    } else if (isInForge()) {
        await scrapeForgeForInventory();
    } else if (isInClanShop()) {
        await scrapeClanShopForInventory();
    } else if (isInClanShopHistory()) {
        await scrapeClanShopHistory();
    } else if (isInClanTeamList()) {
        await scrapeClanTeamList();
    }
};

const isLoggedOut = () => {
    return document.querySelectorAll("a[href='/account/login']").length > 0;
};

const logIn = async () => {
    if (isOnLoginPage()) {
        const emailInput = document.getElementById('input-login');
        const passwordInput = document.getElementById('input-password');
        const rememberMe = document.getElementsByName("input-autologin")[0];
        const loginButton = document.getElementById("login-nm-button");

        await nm2GetUser().then(async (user) => {
            emailInput.value = user.email;
            passwordInput.value = user.password;
            rememberMe.checked = true;

            await sleep(Math.floor(Math.random() * 2) + 1);

            loginButton.click();
        });
    } else {
        goToLoginPage();
    }
};

const goToLoginPage = () => {
    window.location = "https://www.ninjamanager.com/account/login";
};

const showAllItemsInShop = async () => {
   const availableFilter = Array.from(document.getElementsByClassName("c-filter-button")).find(element => element.getAttribute('data-filter') === 'available');
   if (availableFilter && availableFilter.classList.contains('-c-sel')) {
       availableFilter.click();
   }
};

const scrapeClanShopForInventory = async () => {
    const allItemsInShopDB = await nm2GetClanShopItems();
    
    const allItemsIdsInShopByName = {};

    allItemsInShopDB.forEach((item) => allItemsIdsInShopByName[item.name] = item.ids.sort());

    console.log({allItemsIdsInShopByName});
    
    console.log("scraping shop...");
    const inventoryData = scanForClanShopItems().map(element => {
        const picEle = element.getElementsByClassName('c-item__pic')[0];
        const isLegendary = !!element.getAttribute('data-filter')?.split(' ').includes('legendary');
        return {
            ids: JSON.parse(element.getAttribute('data-ids')),
            name: element.getElementsByClassName('c-item__name  a-item-name')[0].textContent,
            img: picEle ? picEle.children[0].getAttribute('src') : undefined,
            tooltip: element.getElementsByClassName('js-item-tooltip')[0].getAttribute('data-url'),
            cost: parseInt(element.getElementsByClassName('c-resource -resource-gems')[0].children[0].textContent),
            hasPriorityPointsResolved: false,
            isLegendary
        };
    });

    const combinedInventoryData = {};

    inventoryData.forEach(data => {
        if (combinedInventoryData[data.name]) {
            combinedInventoryData[data.name].ids = Array.from(new Set([...combinedInventoryData[data.name].ids, ...data.ids]));
        } else {
            combinedInventoryData[data.name] = data;
        }
    })

    console.log({combinedInventoryData});

    const inventoryAsList = Object.values(combinedInventoryData);
    
    for (let i = 0; i < inventoryAsList.length; i++) {
        const item = inventoryAsList[i];
        if (!allItemsIdsInShopByName[item.name] || JSON.stringify(allItemsIdsInShopByName[item.name]) !== JSON.stringify(item.ids.sort())) {
            console.log("Updating item: " + JSON.stringify(item));
            await nm2PutClanShopItem(inventoryAsList[i]);
        }
    }

    const scrapedItemNames = Object.keys(combinedInventoryData);
    const dbItemNames = Object.keys(allItemsIdsInShopByName);
    dbItemNames.forEach(async (dbItemName) => {
        if (!scrapedItemNames.includes(dbItemName)) {
            console.log("removing: " + dbItemName);
            await nm2DeleteClanShopItem(dbItemName);
        }
    });
    
    const uniqueItemNames = Object.keys(combinedInventoryData).sort();
    
    console.log({uniqueItemNames});
    
    const registrationTable = await nm2GetClanShopItemRegistrations();
    
    console.log({registrationTable});
    
    uniqueItemNames.forEach(name => {
        if (!registrationTable.some(registrationData => registrationData.itemname === name)) {
            nm2PutClanShopItemRegistration(name, {needs: [], wants: []});
        }
    });
    
    console.log("done scraping shop.");
};

const scrapeClanTeamList = async () => {
    const clanMembers = scanForClanMembers().map(element => {
        return {
            id: parseInt(element.getAttribute('data-teamid')),
            name: element.getElementsByClassName('c-arena-box__name')[0].children[0].textContent,
        }
    });
    
    const clanMemberIds = clanMembers.reduce((ids, member) => {
        return ids.concat(member.id);
    }, []);
    
    await nm2PutCurrentClanMembers(clanMemberIds);
    
    for (let i = 0; i < clanMembers.length; i++) {
        const member = clanMembers[i];
        await nm2PutTeamLookup(member.id, {name: member.name});
        await sleep(20);
    }
};

const scrapeClanShopHistory = async () => {
    const historyData = scanForClanShopItems().map(element => {
        const userBoxElement = element.getElementsByClassName('c-user-box')[0];
        return userBoxElement ? {
            teamid: parseInt(userBoxElement.getAttribute('data-url')),
            name: element.getElementsByClassName('c-item__name  a-item-name')[0].textContent,
        } : null;
    }).filter((data) => data !== null);

    console.log({historyData});

    const allItemsInShopDB = await nm2GetClanShopItems();
    console.log({allItemsInShopDB});
    const itemCountsByName = {};
    const itemsByName = {};

    allItemsInShopDB.forEach((item) => itemCountsByName[item.name] = item.ids.length);
    allItemsInShopDB.forEach((item) => itemsByName[item.name] = item);
    console.log({itemCountsByName});

    // 1. get snapshot
    // 2. diff snapshot to current and find the previous "head"
    // 3. complete purchases for everything from current to previous head
    // 4. save snapshot of history

    const snapshot = JSON.parse(await nm2GetClanShopHistorySnapshot().then((x) => x.snapshot).catch(e => "[]"));

    const isPreviousHead = (index, snapshot, historyData) => {
        for (let i = index; i < historyData.length - index; i++) {
            const snapshotData = snapshot[i - index] || {teamid: "", name: ""};
            const currentData = historyData[i] || {teamid: "", name: ""};
            if ((snapshotData.teamid + snapshotData.name) !== (currentData.teamid + currentData.name)) {
                return false;
            }
        }

        return true;
    }
    let previousHead = historyData.length - 1;
    for (let i = 0; i < historyData.length; i++) {
        if (!snapshot.length) {
            break;
        }
        if (isPreviousHead(i, snapshot, historyData)) {
            previousHead = i;
            break;
        }
    }

    console.log(`processing 0 - ${previousHead}`);

    const needsAndWants = await nm2GetClanShopItemRegistrations();
    const needsAndWantsLookup = needsAndWants.reduce((lookup, current) => {
        return {
            ...lookup,
            [current.itemname]: {
                needs: current.needs,
                wants: current.wants,
            }
        }
    }, {});

    console.log({needsAndWantsLookup});

    const allPoints = await nm2GetAllPriorityPoints();
    console.log({allPoints});
    const pointsLookup = allPoints.reduce((lookup, current) => {
        return {
            ...lookup,
            [current.teamid]: current.points,
        };
    }, {});
    console.log({pointsLookup});

    const isItem = (item) => {
        return !!item.tooltip?.includes('/item/')
    };

    const isMaterial = (item) => {
        return !!item.tooltip?.includes('/material/');
    };

    const isGen = (item) => {
        return !!item.tooltip?.includes('/genjutsu/');
    };

    const isLegendary = (item) => {
        return !!item.isLegendary;
    };

    const getPointsToDing = (clanItem) => {
        if (!clanItem || !clanItem.cost) {
            return 5;
        }

        const punishmentDeduction = clanItem.cost * 2;

        if (isLegendary(clanItem)) {
            return punishmentDeduction;
        } else if (isItem(clanItem) && clanItem.cost <= 300) {
            return 0;
        } else if (isGen(clanItem) && clanItem.cost <= 400) {
            return 0;
        } else if (isMaterial(clanItem) && clanItem.cost <= 50) {
            return 0;
        } else if (isMaterial(clanItem) && clanItem.cost <= 100) {
            return 1;
        } else if (isMaterial(clanItem) && clanItem.cost <= 125) {
            return 2;
        } else if (isMaterial(clanItem) && clanItem.cost < 200) {
            return 5;
        }

        return punishmentDeduction;
    };

    for (let i = 0; i < previousHead; i++) {
        const itemName = historyData[i].name;
        const teamId = historyData[i].teamid;
        const currentNeedsAndWants = needsAndWantsLookup[itemName];
        const doesTeamNeedOrWantItem = currentNeedsAndWants && (
            currentNeedsAndWants.needs.filter((need) => need.teamid === teamId).length > 0
            || currentNeedsAndWants.wants.filter((want) => want.teamid === teamId).length > 0
        );

        if (doesTeamNeedOrWantItem) {
            // remove the need/want for this team (both on DB and locally in case the same team bought multiple of an item)
            needsAndWantsLookup[itemName].needs = needsAndWantsLookup[itemName].needs.filter(need => need.teamid !== teamId);
            needsAndWantsLookup[itemName].wants = needsAndWantsLookup[itemName].wants.filter(want => want.teamid !== teamId);
            console.log("REMOVING NEED/WANT");
            await nm2PutClanShopItemRegistration(itemName, {
                needs: needsAndWantsLookup[itemName].needs,
                wants: needsAndWantsLookup[itemName].wants,
            });
        } else {
            // the team never made a claim to the item, so ding them some points
            pointsLookup[teamId] = pointsLookup[teamId] - getPointsToDing(itemsByName[itemName]);
            console.log("DING");
            await nm2PutPriorityPoints(teamId, {points: pointsLookup[teamId]});
        }

        if (itemCountsByName[itemName] > 1) {
            // there are duplicates.
            itemCountsByName[itemName] = itemCountsByName[itemName]  - 1;
        } else {
            console.log("FINAL ITEM DETECTED - REMOVING");
            // this is the only one left in the shop, so we should resolve points.
            // anyone who has need/want should get their points back.
            // remove the registration entry for the item
            const allReimbursements = needsAndWantsLookup[itemName] ? needsAndWantsLookup[itemName].needs.concat(needsAndWantsLookup[itemName].wants) : [];

            for (let k = 0; k < allReimbursements.length; k++) {
                const reimbursement = allReimbursements[k];
                await nm2PutPriorityPoints(reimbursement.teamid, {points: pointsLookup[reimbursement.teamid] + reimbursement.points});
                await sleep(10);
            }

            await nm2DeleteClanShopItemRegistration(itemName);
        }

        await sleep(10);
    }

    await nm2PutClanShopHistorySnapshot(JSON.stringify(historyData));
};

const scrapeForgeForInventory = async () => {
    const inventory = {};
    scanForForgeItems().forEach(itemInForge => {
        const itemElement = itemInForge.getElementsByTagName("div")[0];
        if (itemElement) {
            const dataUrl = itemElement.getAttribute('data-url');
            if (dataUrl) {
                const dataUrlParts = dataUrl.split('/');
                const name = dataUrlParts[dataUrlParts.length - 1];
                const totalElement = itemElement.getElementsByClassName("c-item__amount")[0];
                if (totalElement) {
                    inventory[name] = parseInt(totalElement.innerHTML);
                }
            }
        }
    });

    console.log({inventory});

    nm2PutInventory(inventory).catch((error) => console.log(error));
};

const beginWorldLoops = async (worldEnergyCap, mission) => {
    await recordItems(scanForItems());

    const missions = document.getElementsByClassName("c-mission-box");

    for (let i = 0; i < missions.length; i++) {
        if (parseInt(missions[i].getAttribute("data-missionid")) === parseInt(mission)){

            let missionButton = missions[i].getElementsByClassName("c-button -c-icon -icon-challenge -color-yes  js-goto-world")[0];

            await worldLooper(missionButton, worldEnergyCap);
            break;
        }
    }
};

const beginArenaLoops = async (arenaEnergyCap) => {
    const teamScore = getTeamScore();
    const teamsLists = document.getElementsByClassName("p-arena-list  js-arena-list");

    if (teamsLists[0]) {
        const teamsHere = teamsLists[0];
        
        const threeDayAverage = await getAvgExpLookup(teamsHere, 3);
        const mostRecentGlobal = await getMostRecentExp(teamsHere);

        const sorted3DayAverageExpList = Object.entries(threeDayAverage).sort(([,a],[,b]) => b-a).map((entry) => [parseInt(entry[0]), entry[1]]);
        const sortedMostRecentGlobalExpList = Object.entries(mostRecentGlobal).sort(([,a],[,b]) => b-a).map((entry) => [parseInt(entry[0]), entry[1]]);

        console.log(sorted3DayAverageExpList);
        console.log(sortedMostRecentGlobalExpList);

        const longerListLength = Math.max(sorted3DayAverageExpList.length, sortedMostRecentGlobalExpList.length);
        const sortedExpList = [];

        for (let i = 0; i < longerListLength; i++) {
            sortedMostRecentGlobalExpList[i] && sortedExpList.push(sortedMostRecentGlobalExpList[i]);
            sorted3DayAverageExpList[i] && sortedExpList.push(sorted3DayAverageExpList[i]);
        }

        console.log({sortedExpList});

        await arenaLooper(teamsHere, sortedExpList, teamScore, arenaEnergyCap, 0);
    }
};

const getEnemyArenaIdFromDiv = (div) => {
    try {
        return parseInt(div.getElementsByClassName('js-team-tooltip')[0].getAttribute('data-url'))
    } catch (err) {
        return null;
    }
};

const getMostRecentExp = async (teamsHere) => {
    const teamDivs = [...teamsHere.getElementsByClassName("c-arena-box")];

    const teamIds = teamDivs.map((div) => {
        return getEnemyArenaIdFromDiv(div);
    }).filter((id) => id !== null && id !== 101);

    const allReplays = await nm2GetAllReplays();

    const replaysHere = allReplays.filter((replay) => {
        return teamIds.includes(replay.teamid);
    });

    const mostRecent = {};

    for(index in teamIds) {
        const id = teamIds[index];
        mostRecent[id] = allReplays.filter(replay => replay.teamid === id)
            .reduce((a, b) => b.lastSeen > a.lastSeen ? b : a, {lastSeen: 0, exp: 0}).exp;
        if (mostRecent[id] === 0) {
            delete mostRecent[id];
        }
    }

    return mostRecent;
};

const getAvgExpLookup = async (teamsHere, days = undefined) => {
    // unwind htmlcollection into array
    const teamDivs = [...teamsHere.getElementsByClassName("c-arena-box")];

    const teamIds = teamDivs.map((div) => {
        return getEnemyArenaIdFromDiv(div);
    }).filter((id) => id !== null && id !== 101);

    console.log({teamIds});

    const allReplays = await nm2GetAllReplays();
    console.log({allReplays});

    const result = {};
    for(index in teamIds) {
        const id = teamIds[index];

        const foundReplays = allReplays.filter((replay) => {
            const daysInMillis = days * 24 * 60 * 60 * 1000;
            if (days && replay.lastSeen < Date.now() - daysInMillis) {
                return false;
            }

            return replay.teamid === id;
        });

        if (foundReplays.length > 0) {
            result[id] = foundReplays.reduce((sum, foundReplay) => sum += foundReplay.exp, 0) / foundReplays.length;
        } else {
            result[id] = 0;
        }
    }

    return result;
}

const getTeamScore = () => {
    return document
        .getElementsByClassName("header-team__details-rating  js-header-rating")[0]
        .getElementsByTagName("span")[0]
        .innerText.replace(",", "");
};

const sleep = (ms) => {
  return new Promise(resolve => setTimeout(resolve, ms));
};

const addItemToHuntedItemsList = (itemElement) => {
    const dataUrlParts = itemElement.getAttribute('data-url').split('/');
    const name = dataUrlParts[dataUrlParts.length - 1];
    const dropRate = parseFloat(itemElement.getElementsByClassName("c-item__drop")[0].getElementsByTagName("b")[0].innerHTML);
    const id = parseInt(itemElement.parentElement.parentElement.parentElement.getAttribute("data-missionId"));
    const area = getArea();

    nm2PutItem(id, {area, dropRate, name}).then((success) => {
        if (success) {
            console.log(`saved item: ${{id, area, dropRate, name}}`);
        } else {
            console.log(`failed to save item: ${{id, area, dropRate, name}}`)
        }
    });
};

const sanitizeItems = (items) => {
    return sortItems(uniqueFromArray(removeBadRecords(items)));
};

const sortItems = (uniqueItems) => {
    return uniqueItems.sort(function(a, b) {
        if (a.name > b.name) return 1;
        if (b.name > a.name) return -1;

        return 0;
    });
};

const removeBadRecords = (records) => {
    return records.filter((item) => {
        return item.area !== null && item.area !== undefined && item.area !== "" && typeof item.id === "number";
    });
};

const uniqueFromArray = (array) => {
    return Array.from(
        new Set(array.map(a => a.id))
    ).map(id => {
        return array.find(a => a.id === id)
    });
};

const getArea = () => {
    let pathParts = window.location.toString().split('/');
    let maxIndex = pathParts.length - 1;
    let areaIndex = 0 ;

    pathParts.forEach(function(part, index) {
        if (part === 'area') {
            areaIndex = index;
        }
    });

    return pathParts[Math.min(areaIndex + 1, maxIndex)];
};

const recordItems = async (foundItemDivs) => {
    console.log("collecting items...");
    for (let i = 0; i < foundItemDivs.length; i++) {
        addItemToHuntedItemsList(foundItemDivs[i]);
        await sleep(500);
    }
    console.log("Done collecting items.");
};

const scanForItems = () => {
    return Array.from(document.getElementsByClassName("c-item -size-s h-item-material-1 -type-material -width-drop   js-item-tooltip"));
};

const scanForForgeItems = () => {
    return Array.from(document.getElementsByClassName("pc-forge-ingredient   js-ingredient"));
};

const scanForClanShopItems = () => {
    return Array.from(document.getElementsByClassName("c-shop-box  js-shop-item"));
};

const scanForClanMembers = () => {
    return Array.from(document.getElementsByClassName("c-arena-box"));
};

const isInForge = () => {
    return new RegExp("^.*:\/\/www\.ninjamanager\.com\/forge.*$").test(window.location.href);
};

const isInClanArea = () => {
    return new RegExp("^.*:\/\/www\.ninjamanager\.com\/clans.*$").test(window.location.href);
};

const isInClanShop = () => {
    return window.location.href === 'https://www.ninjamanager.com/clans/shop';
};

const isInClanShopHistory = () => {
    return window.location.href === 'https://www.ninjamanager.com/clans/shop?history';
};

const isInClanTeamList = () => {
    return window.location.href === 'https://www.ninjamanager.com/clans/edit-positions';
};

const isInMyTeamMenu = () => {
    return window.location.href.split('/').at(-1) === 'myteam';
};

const isInReplay = () => {
    return new RegExp("^.*:\/\/www\.ninjamanager\.com\/replays\/watch\/.*$").test(window.location.href);
};

const isInNM2 = () => {
    return new RegExp("^.*:\/\/www\.ninjamanager\.com\/.*$").test(window.location.href);
};

const isInArena = () => {
    return new RegExp("^.*:\/\/www\.ninjamanager\.com\/arena.*$").test(window.location.href);
};

const isInWorld = () => {
    return new RegExp("^.*:\/\/www\.ninjamanager\.com\/world.*$").test(window.location.href);
};

const isOnLoginPage = () => {
    return new RegExp("^.*:\/\/www\.ninjamanager\.com\/account\/login.*$").test(window.location.href);
};

const navigateToNewLocation = (area) => {
    if (isInClanArea()) {
        clanNavigation();
    } else if (isInMyTeamMenu() || isInReplay()) {
        window.location = "https://www.ninjamanager.com/myteam";
    } else {
        nonClanNavigation(area);
    }
};

const clanNavigation = () => {
    if (isInClanShop()) {
        window.location = "http://www.ninjamanager.com/clans/shop?history";
    } else if (isInClanShopHistory()) {
        window.location = 'https://www.ninjamanager.com/clans/edit-positions';
    } else {
        window.location = "http://www.ninjamanager.com/clans/shop";
    }
};

const nonClanNavigation = (area) => {
    if (!isInForge()) {
        window.location = "http://www.ninjamanager.com/forge";
    } else {
        nm2GetSettings().then(settings => {
            if (settings.toArena) {
                nm2UpdateSettings("toArena", false).catch(err => console.error(err));
                window.location = "http://www.ninjamanager.com/arena";
            } else {
                nm2UpdateSettings("toArena", true).catch(err => console.error(err));
                window.location = "http://www.ninjamanager.com/world/area/" + area;
            }
        })
    }
};

const countdownToNewLocation = (timeToWait, area, invokeTime = Date.now(), previousTime = Date.now()) => {
    setTimeout(function () {
        const now = Date.now();
        const timeSpent = now - invokeTime;
        const seconds = Math.floor(timeSpent / 1000);
        const minutes = Math.floor(seconds / 60);
        const formattedSeconds = seconds % 60;
        console.log({
            timeElapsed: `${pad(minutes, 2)}:${pad(formattedSeconds, 2)}`,
            timeRemaining: `${pad(timeToWait - minutes, 2)}:${pad(59 - formattedSeconds, 2)}`,
            msElapsed: `${now - previousTime}`
        });

        if (minutes >= timeToWait) {
            navigateToNewLocation(area);
        } else {
            countdownToNewLocation(timeToWait, area, invokeTime, now);
        }
    }, 100);
};

const worldLooper = async (missionButton, energyCap) => {
    await sleep(5000);

    if (getCurrentWorldEnergy() > energyCap) {
        missionButton.click();
        await worldLooper(missionButton, energyCap);
    }
};

const arenaLooper = async (teamList, sortedExpList, teamScore, energyCap, index) => {
    const listOfDivs = teamList.getElementsByClassName("c-arena-box");
    console.log({listOfDivs});
    const priorityDivs = sortedExpList.filter(expEntry => expEntry[1] > 0).map(expEntry => [...listOfDivs].filter(div => expEntry[0] === getEnemyArenaIdFromDiv(div))[0])
    console.log({priorityDivs});

    await sleep(2000);

    closeDialogue();

    await battleAllEnemiesInList(teamScore, energyCap, priorityDivs, true);
    await battleAllEnemiesInList(teamScore, energyCap, listOfDivs);
};

const battleAllEnemiesInList = async (teamScore, energyCap, enemyDivs, allowHigherScores = false) => {
    for (index in enemyDivs) {
            const targetDiv = enemyDivs[index];
            console.log({targetDiv});
        if (getCurrentArenaEnergy() > energyCap) {
            const targetDiv = enemyDivs[index];
            const enemyScore = parseInt(targetDiv.getElementsByClassName("c-arena-box__rating")[0].innerText.replace(",", ""));
            const fightButtons = targetDiv.getElementsByClassName("js-challenge-team");

            const allowBasedOnScore = allowHigherScores || (enemyScore < parseInt(teamScore) && enemyScore !== 500);

            if (fightButtons.length > 0 && allowBasedOnScore) {
                fightButtons[0].click();
            }

            await sleep(2000);

            closeDialogue();

            await sleep(1000);
        }
    }
}

const closeDialogue = () => {
    const closeButton = document.getElementsByClassName("c-overlay-message__close  c-button -color-no");
    if (closeButton.length > 0){
        closeButton[0].click();
    }
}

const getCurrentArenaEnergy = () => {
    const arenaEnergy = document.getElementsByClassName("header-team__bar header-team__bar-ae  c-bar -type-ae  js-header-energy-arena");
    return arenaEnergy ? parseInt(arenaEnergy[0].getElementsByClassName("c-bar__text-cur")[0].innerText) : 0;
};

const getCurrentWorldEnergy = () => {
    const worldEnergy = document.getElementsByClassName("header-team__bar header-team__bar-we  c-bar -type-we  js-header-energy-world");
    return worldEnergy ? parseInt(worldEnergy[0].getElementsByClassName("c-bar__text-cur")[0].innerText) : 0;
};

const pad = (num, size) => {
    let s = num+"";
    while (s.length < size) s = "0" + s;
    return s;
};
