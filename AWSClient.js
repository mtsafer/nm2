const cryptoPassword = '&</EzAV@`V8t:P5FU2r]q@v/fa(M2<Tx/ApJ{a4g'; // this can NEVER change

// AWS comes from minified aws-sdk because chrome extensions can't use node
AWS.config.update({region: 'us-west-2'});
AWS.config.update({dynamoDbCrc32: false});

const credentials = new AWS.Credentials('AKIA4P7QHKU5L5MBGMDZ', 'u/MP6wslDs7VUoQP6qTTylGmzgQ+162qOFFG6Y5z');

AWS.config.update({credentials});

// Create DynamoDB document client
const nm2DocClient = new AWS.DynamoDB.DocumentClient({apiVersion: '2012-08-10'});

const UserSingleton = 'nm2_user';
const SettingsSingleton = 'nm2_settings';
const SingletonTable = 'singleton';
const ReplaysTable = 'replays';
const ItemsTable = 'nm2_items';
const InventorySingleton = 'nm2_inventory';
const ClanShopHistorySnapshotSingleton = 'nm2_clan_shop_history_snapshot';
const ClanShopItemTable = 'nm2_clan_shop_items';
const PurchaseSnapshotTable = 'nm2_clan_shop_purchase_snapshot';
const ClanShopInventoryTable = 'nm2_clan_shop_inventory';
const ClanShopHistoryTable = 'nm2_clan_shop_history';
const ClanMembersTable = 'nm2_clan_members';
const TeamLookupTable = 'nm2_team_lookups';
const ClanPriorityPointsTable = 'nm2_clan_priority_points';
const ClanShopRegistrationTable = 'nm2_clan_shop_priority_registration';
const SicMundusClanName = 'sicmundus';

const nm2Put = async (TableName, Item) => {
    return nm2DocClient.put({TableName, Item}).promise().then(() => true).catch(() => false);
};

const nm2Get = async (TableName, Key) => {
    return nm2DocClient.get({TableName, Key}).promise().then((data) => {
        return data.Item;
    });
};

const nm2GetAll = async(TableName) => {
    return nm2DocClient.scan({TableName,}).promise().then((data) => {
        return data.Items;
    });
};

const nm2Del = async (TableName, Key) => {
    return nm2DocClient.delete({TableName, Key}).promise().then(() => true).catch(() => false);
};

const nm2Update = async (TableName, Key, updateFieldName, newValue) => {
    const params = {
        TableName,
        Key,
        UpdateExpression: `set ${updateFieldName} = :v`,
        ExpressionAttributeValues: {
            ':v' : newValue,
        }
    };

    nm2DocClient.update(params).promise().then(() => true).catch(() => false);
};

const nm2GetUser = async () => {
    return nm2GetSingleton(UserSingleton).then(item => {
        const decryptedPassword = CryptoJS.AES.decrypt(item.password, cryptoPassword).toString(CryptoJS.enc.Utf8);
        return {
            ...item,
            password: decryptedPassword,
        }
    });
};

const nm2PutSettings = async (settings) => {
    return nm2PutSingleton({name: SettingsSingleton, ...settings});
};

const nm2GetSettings = async () => {
    return nm2GetSingleton(SettingsSingleton);
};

const nm2GetSingleton = async (name) => {
    return nm2Get(SingletonTable, {name});
};

const nm2PutSingleton = async (Item) => {
    return nm2Put(SingletonTable, Item);
};

const nm2DelSingleton = async (name) => {
    return nm2Del(SingletonTable, name);
};

const nm2GetAllReplays = async () => {
    return nm2GetAll(ReplaysTable);
}

const nm2PutReplay = async (teamid, replayid, exp) => {
    return nm2Put(ReplaysTable, {teamid, replayid, exp, lastSeen: Date.now()});
}

const nm2GetReplaysByTeamId = async (teamId, Limit = 10, ScanIndexForward = false) => {
    return nm2DocClient.query({
        KeyConditionExpression: 'teamid = :teamId AND replayid > :zero',
        ExpressionAttributeValues: {
            ':teamId': teamId,
            ':zero': 0,
        },
        TableName: ReplaysTable,
        Limit,
        ScanIndexForward,
    }).promise()
}

const nm2PutItem = async (itemid, data) => {
    return nm2Put(ItemsTable, {itemid, ...data});
};

const nm2PutItems = async (items) => {
    items.forEach((item) => {
        const itemId = item.id;
        delete item.id;
        nm2PutItem(itemId, item);
    });
};

const nm2DeleteItem = async (itemid) => {
    return nm2Del(ItemsTable, {itemid});
};

const nm2GetItem = async (itemid) => {
    return nm2Get(ItemsTable, {itemid});
};

const nm2GetAllItems = async () => {
    return nm2GetAll(ItemsTable);
};

const nm2PutClanShopItem = async (item) => {
  return nm2Put(ClanShopItemTable, {...item});
};

const nm2DeleteClanShopItem = async (name) => {
    return nm2Del(ClanShopItemTable, {name});
  };

const nm2PutClanPurchaseSnapshot= async (snapshot) => {
  return nm2Put(PurchaseSnapshotTable, {...snapshot});
};

const nm2PutClanShopInventory = async (inventoryIds) => {
  return nm2Put(ClanShopInventoryTable, {clan: SicMundusClanName, currentInventory: inventoryIds});
};

const nm2PutClanShopHistory = async (teamid, history) => {
  return nm2Put(ClanShopHistoryTable, {teamid, history});
};

const nm2GetClanShopHistory = async () => {
  return nm2GetAll(ClanShopHistoryTable);
};

const nm2GetClanShopInventory = async () => {
  return nm2Get(ClanShopInventoryTable, {clan: SicMundusClanName});
};

const nm2GetClanShopItemRegistrations = async () => {
  return nm2GetAll(ClanShopRegistrationTable);
};

const nm2PutClanShopItemRegistration = async (itemname, data) => {
  return nm2Put(ClanShopRegistrationTable, {itemname, ...data});
};

const nm2DeleteClanShopItemRegistration = async (itemname) => {
  return nm2Del(ClanShopRegistrationTable, {itemname});
};

const nm2GetClanShopItems = async () => {
  return nm2GetAll(ClanShopItemTable);
};

const nm2GetClanShopHistorySnapshot = async () => {
    return nm2GetSingleton(ClanShopHistorySnapshotSingleton);
};

const nm2PutClanShopHistorySnapshot = async (snapshot) => {
    return nm2PutSingleton({name: ClanShopHistorySnapshotSingleton, snapshot});
};

const nm2PutCurrentClanMembers = async (currentMembers) => {
  return nm2Put(ClanMembersTable, {clan: SicMundusClanName, currentMembers})
};

const nm2PutTeamLookup = async (teamid, data) => {
  return nm2Put(TeamLookupTable, {teamid, ...data})
};

const nm2PutPriorityPoints = async (teamid, data) => {
  return nm2Put(ClanPriorityPointsTable, {teamid, ...data})
};

const nm2GetPriorityPoints = async (teamid) => {
    return nm2GetSingleton(InventorySingleton);
};

const nm2GetAllPriorityPoints = async () => {
  return nm2GetAll(ClanPriorityPointsTable)
};

const nm2PutInventory = async (inventory) => {
    return nm2PutSingleton({name: InventorySingleton, ...inventory});
};

const nm2GetInventory = async () => {
    return nm2GetSingleton(InventorySingleton);
};

const nm2UpdateSettings = async (updateFieldName, newValue) => {
    return nm2GetSettings().then((settings) => (
        nm2PutSettings({...settings, [updateFieldName]: newValue})
    ));
};
