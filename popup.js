$( document ).ready(function() {
    $("#response").hide();
    $("#save").click(save);
    populateFields();
    pupulateItemFinder();
    document.getElementById("main_menu_tab").addEventListener('click', function(event){openTab(event, 'main_menu')});
    document.getElementById("item_finder_tab").addEventListener('click', function(event){openTab(event, 'item_finder')});
    document.getElementById("main_menu_tab").click();
});

function save() {
    const onCheckbox = $("#isOn");
    const toArenaCheckbox = $("#toArena");
    const isOn = onCheckbox.prop("checked");
    const area = $("#area").val();
    const mission = $("#mission").val();
    const worldEnergyCap = $("#worldEnergyCap").val();
    const arenaEnergyCap = $("#arenaEnergyCap").val();
    const stayLength = $("#stayLength").val();
    const toArena = toArenaCheckbox.prop("checked");
    nm2PutSettings({isOn, area, mission, worldEnergyCap, arenaEnergyCap, stayLength, toArena}).then(() => {
        showResponse("saved!");
    });
}

function showResponse(response) {
    const responseEle = $("#response");
    responseEle.html(response);
    responseEle.show();
    responseEle.fadeOut(3000, function(){});
}

function populateFields() {
    nm2GetSettings().then((data) => {
        $("#isOn").prop("checked", data.isOn);
        $("#area").prop("value", data.area);
        $("#mission").prop("value", data.mission);
        $("#worldEnergyCap").prop("value", data.worldEnergyCap);
        $("#arenaEnergyCap").prop("value", data.arenaEnergyCap);
        $("#stayLength").prop("value", data.stayLength);
        $("#toArena").prop("checked", data.toArena);
    });
}

const itemSorter = (a, b) => {
    const nameA = a.name.toUpperCase(); // ignore upper and lowercase
    const nameB = b.name.toUpperCase(); // ignore upper and lowercase
    if (nameA < nameB) {
        return -1;
    }
    if (nameA > nameB) {
        return 1;
    }

    return 0;
};

function pupulateItemFinder() {
    nm2GetAllItems().then(items => {
        nm2GetInventory().then(inventory => {
            if (items) {
                items.sort(itemSorter);
                items.forEach(function(huntedItem){
                    addItemToItemFinder(
                        huntedItem.name,
                        huntedItem.area,
                        huntedItem.dropRate,
                        huntedItem.itemid,
                        inventory[huntedItem.name] ? parseInt(inventory[huntedItem.name]) : 0
                    );
                });
            } else {
                addItemToItemFinder(
                    "No items found :(",
                    null,
                    null,
                    null,
                    null
                );
            }
        })
    });
}

function openTab(evt, tabName) {
  // Declare all letiables
  let i, tabcontent, tablinks;

  // Get all elements with class="tabcontent" and hide them
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }

  // Get all elements with class="tablinks" and remove the class "active"
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }

  // Show the current tab, and add an "active" class to the button that opened the tab
  document.getElementById(tabName).style.display = "block";
  evt.currentTarget.className += " active";
}

function huntItem(area, id) {
    nm2UpdateSettings("area", area).then(() => {
        nm2UpdateSettings("mission", id).then(() => {
            showResponse("Hunting for in " + area + " for " + id);
            populateFields();
        })
    })
}

function addItemToItemFinder(name, area, drop_rate, id, inventory) {
    let tr = document.createElement("tr");

    if (name !== null) {
        let td_name =  document.createElement("td");
        let link =  document.createElement("a");
        link.href = "https://www.ninjamanager.com/world/area/" + area;
        link.target = "_blank";
        link.appendChild(document.createTextNode(name));
        td_name.appendChild(link);

        let td_image =  document.createElement("td");
        let image =  document.createElement("img");
        image.src = "https://www.ninjamanager.com/img/material/small/" + name + ".png";
        td_image.appendChild(image);

        tr.appendChild(td_name);
        tr.appendChild(td_image);
    }

    if (drop_rate || drop_rate === 0) {
        let td_drop = document.createElement("td");
        td_drop.appendChild(document.createTextNode(drop_rate + "%"));

        tr.appendChild(td_drop);
    }

    if (inventory || inventory === 0) {
        let td_inv = document.createElement("td");
        td_inv.appendChild(document.createTextNode(inventory));

        tr.appendChild(td_inv);
    }

    if (area && id) {
        let td_hunt =  document.createElement("td");
        let button =  document.createElement("button");
        button.class = "item_hunt";
        button.id = "item_" + id;
        button.data_area = area;
        button.appendChild(document.createTextNode("Hunt?"));

        button.addEventListener('click', function(event){huntItem(area, id)});

        td_hunt.appendChild(button);

        tr.appendChild(td_hunt);
    }

    document.getElementById("item_finder_table").appendChild(tr);
}